
const express = require('express');
const axios  = require("axios");
const { get } = require('express/lib/request');
const res = require('express/lib/response');
const bodyParser = require("body-parser");

 const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//  -H 'Content-Type: application/json' \
//   -H 'X-User-Email: dev+tayfur@onlim.com' \
//   -H 'X-User-Organization-Id: 764' \
//   -H 'X-User-Token: PsHt_Yign3FgYJR1h94y' \
//   -H 'cache-control: no-cache
async function getDataFromApi(){
    try {
        const response = await axios.get("https://app-staging.onlim.com/api/bms/chatbot_api/v1/local_businesses.json",{
            headers:{
                'X-User-Token': "PsHt_Yign3FgYJR1h94y",
                'X-User-Organization-Id': "764",
                'X-User-Email': "dev+tayfur@onlim.com",
            }
        }) 
    
        dataStore=response.data.local_businesses
        return response.data
    } catch (error) {
        return error
    }
}
let dataStore ={}
getDataFromApi()
// this endopoint is for response all data already stored in dataStore
app.get("/getData",async (req,res)=>{
    res.json(dataStore)
})

app.get("/:id",async (req,res)=>{
    const id = req.params.id
    const dataArray = [dataStore]
    let responseArray
    for (let index = 0; index < dataStore.length; index++) {
        if (dataStore[index]._id===id) {   
            const element = dataStore[index]
            responseArray=[element]
        }
 
    }
    if (!responseArray) {
       res.status(404).json("id is empty") 
    }
    else{
    res.json(responseArray)
    }
    })
    // its can update email field for a specific id
app.put("/update/:id",async(req,res)=>{
    const id = req.params.id
    const { email } = req.body;
    const dataArray = [dataStore]
    let responseArray
    for (let index = 0; index < dataStore.length; index++) {
        if (dataStore[index]._id===id) {   
            const element = dataStore[index]
            element.email=email
            responseArray=[element]

        }
 
    }
    if (!responseArray) {
       res.status(404).json("id is empty") 
    }
    else{
    res.json(responseArray)
    }
})
// its can delete the data from the array with the id

app.delete("/delete/:id",async(req,res)=>{
    const id = req.params.id;
    for (let index = 0; index < dataStore.length; index++) {
        if (dataStore[index]._id===id) {   
            const element = dataStore[index]
            dataStore.splice(index,1)
            res.json("deleted"+element._id)

        }

    }

})
app.listen(3000,()=>console.log("server is running"))